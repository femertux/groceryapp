//
//  ProductCell.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import UIKit

public protocol ProductCellDelegate {
    func add(id: String)
    func remove(id: String)
}

class ProductCell: UITableViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var quantityStack: UIStackView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var quantityLabel: UILabel!
    
    private var quantity = 0
    private var productId = ""
    var delegate: ProductCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        productNameLabel.textColor = .primaryTextColor
        productNameLabel.textAlignment = .left
        productNameLabel.numberOfLines = 1
        productNameLabel.lineBreakMode = .byTruncatingTail
        productNameLabel.font = productNameLabel.font
        
        productDescriptionLabel.textColor = .secondaryTextColor
        productDescriptionLabel.textAlignment = .left
        productDescriptionLabel.numberOfLines = 1
        productDescriptionLabel.lineBreakMode = .byTruncatingTail
        productDescriptionLabel.font = productDescriptionLabel.font
        
        productPriceLabel.textColor = .primaryColor
        productPriceLabel.textAlignment = .left
        productPriceLabel.numberOfLines = 1
        productPriceLabel.lineBreakMode = .byTruncatingTail
        productPriceLabel.font = productPriceLabel.font
        
        productPriceLabel.textColor = .primaryColor
        productPriceLabel.textAlignment = .left
        productPriceLabel.numberOfLines = 1
        productPriceLabel.lineBreakMode = .byTruncatingTail
        productPriceLabel.font = productPriceLabel.font
        
        productImage.contentMode = .scaleAspectFit
        productImage.layer.cornerRadius = 6.0
        productImage.layer.masksToBounds = true
        
        quantityLabel.textColor = .primaryTextColor
        quantityLabel.textAlignment = .center
        quantityLabel.numberOfLines = 1
        quantityLabel.lineBreakMode = .byTruncatingTail
        quantityLabel.font = quantityLabel.font
        
        setUpButton(addButton,
                    text: "+",
                    textColor: .white,
                    buttonColor: .primaryColor,
                    buttonBorderColor: .primaryColor,
                    font: (addButton.titleLabel?.font)!)
        
        setUpButton(removeButton,
                    text: "-",
                    textColor: .white,
                    buttonColor: .primaryColor,
                    buttonBorderColor: .primaryColor,
                    font: (removeButton.titleLabel?.font)!)
    }

    func setProduct(product: Product, edit: Bool = false){
        if let title = product.title,
           let detail = product.unitMeasure,
           let id = product.id,
           let price = product.price,
           let image = product.image {
            productNameLabel.text = title
            productDescriptionLabel.text = detail
            productPriceLabel.text = "$ \(price)"
            productImage.loadFrom(imageUrl: image)
            
            self.quantity = product.quantity ?? 0
            self.productId = id
            updateQuantity()
            quantityStack.isHidden = !edit
        }
    }
    
    @IBAction func add(_ sender: Any) {
        quantity += 1
        updateQuantity()
        delegate?.add(id: productId)
    }
    
    @IBAction func remove(_ sender: Any) {
        guard quantity > 0 else { return }
        quantity -= 1
        updateQuantity()
        delegate?.remove(id: productId)
    }
    
    private func updateQuantity(){
        quantityLabel.text = String(format: "%02d", quantity)
    }
    
    private func setUpButton(_ button: UIButton,
                             text: String = "",
                             textColor: UIColor = .white,
                             buttonColor: UIColor = .black,
                             buttonBorderColor: UIColor = .black,
                             font: UIFont){
        button.setTitle(text, for: .normal)
        button.setTitleColor(textColor, for: .normal)
        button.setTitleColor(textColor.withAlphaComponent(0.5), for: .highlighted)
        button.titleLabel?.font = UIFontMetrics(forTextStyle: .subheadline).scaledFont(for: font)
        button.backgroundColor = buttonColor
        button.layer.cornerRadius = 8.0
        button.layer.borderWidth = 1.0
        button.layer.borderColor = buttonBorderColor.cgColor
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        button.layer.shadowOpacity = 0.4
        button.layer.shadowRadius = 4.0
        button.layer.masksToBounds = false
    }
}
