//
//  ProductResponse.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

// MARK: - ProductsResponse
struct ProductsResponse: Codable {
    let products: [Product]?
    let message: String?
}


// MARK: - ProductResponse
struct ProductResponse: Codable {
    let product: Product?
    let message: String?
}
