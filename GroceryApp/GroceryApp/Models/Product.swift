//
//  Product.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import Foundation

// MARK: - Team
struct Product: Codable {
    let id, title, detail: String?
    let unitMeasure, image: String?
    let price, tax: Double?
    var quantity: Int?
}

#if DEBUG
// MARK: - Example Item
extension Product {
    
    static var exampleProduct: Product {
        Product(
            id:"1",
            title:"Yellow Bananas",
            detail:"The banana is an anytime, year-round snack. We like them fully yellow with just a dusting of brown freckles. But super-ripe, meltingly sweet bananas and firmer greenish ones have their fans too. Slice them onto cereal or pancakes, fold into fruit salad, blend into smoothies, and bake into muffins. Heat brings out bananas' creamy sweetness. Try baking, broiling, or sautéing them with butter and sugar for a luscious dessert.",
            unitMeasure:"2-3lb",
            image:"https://www.freshdirect.com/media/images/product/fruit_3/fru_banana_bnch_ea_j.jpg?lastModify=2021-03-26&publishId=3233",
            price:1.99,
            tax:0.0,
            quantity: 1
        )
    }
}
#endif
