//
//  UserDb.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

struct UserBd: Codable {
    let username: String
    let password: String
    var biometric: Bool = false
}
