//
//  UIImageView+Extension.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import Foundation
import UIKit

extension UIImageView {
    func loadFrom(imageUrl: String) {
        guard let url = URL(string: imageUrl) else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            // Error handling...
            guard let imageData = data else { return }
            
            DispatchQueue.main.async {
                self.image = UIImage(data: imageData)
            }
        }.resume()
    }
}

