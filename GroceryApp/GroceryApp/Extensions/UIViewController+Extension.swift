//
//  UIViewController+Extension.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import Foundation
import UIKit

extension UIViewController {

    static let loading = LoadingController(nibName: "LoadingController", bundle: nil)
    static let loadingAnimationDuration: Int = 500
    
    func setUpLabel(_ label: UILabel,
                    text: String = "",
                    textColor: UIColor = .black,
                    textAlignment: NSTextAlignment = .left,
                    font: UIFont,
                    numberOfLines: Int = 1){
        
        label.text = text
        label.textColor = textColor
        label.textAlignment = textAlignment
        label.font = font
        label.numberOfLines = numberOfLines
        label.lineBreakMode = .byTruncatingTail
        
    }
    
    func setUpButton(_ button: UIButton,
                     text: String = "",
                     textColor: UIColor = .white,
                     buttonColor: UIColor = .black,
                     buttonBorderColor: UIColor = .black,
                     font: UIFont){
        button.setTitle(text, for: .normal)
        button.setTitleColor(textColor, for: .normal)
        button.setTitleColor(textColor.withAlphaComponent(0.5), for: .highlighted)
        button.titleLabel?.font = UIFontMetrics(forTextStyle: .subheadline).scaledFont(for: font)
        button.backgroundColor = buttonColor
        button.layer.cornerRadius = 8.0
        button.layer.borderWidth = 1.0
        button.layer.borderColor = buttonBorderColor.cgColor
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        button.layer.shadowOpacity = 0.4
        button.layer.shadowRadius = 4.0
        button.layer.masksToBounds = false
    }
    
    
    func setUpTextField(_ textField: UITextField,
                        text: String = "",
                        textColor: UIColor = .black,
                        font: UIFont,
                        placeholder: String,
                        leftImage: String? = nil,
                        keyboardType: UIKeyboardType = .default,
                        returnKeyType: UIReturnKeyType = .next,
                        clearButton: UITextField.ViewMode = .never,
                        isSecureTextEntry: Bool = false){
        textField.text = text
        textField.textColor = textColor
        textField.font = UIFontMetrics(forTextStyle: .subheadline).scaledFont(for: font)
        textField.backgroundColor = .white
        textField.layer.cornerRadius = 4.0
        textField.layer.borderWidth = 1.0
        textField.layer.borderColor = UIColor.borderColor.cgColor
        textField.textAlignment = .left
        textField.keyboardType = keyboardType
        textField.returnKeyType = returnKeyType
        textField.isSecureTextEntry = isSecureTextEntry
        
        textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [
            .foregroundColor: UIColor.secondaryTextColor,
            .font: UIFontMetrics(forTextStyle: .subheadline).scaledFont(for: font)
        ])
        
        textField.setLeftPaddingPoints(16.0)
        
        guard let leftImage = leftImage else { return }
        textField.setLeftImage(imageName: leftImage)
    }
    
    func showAlertDialog(title: String, message: String){
        let dialogMessage = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in })
        dialogMessage.addAction(ok)
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    func showLoading(){
        UIViewController.loading.providesPresentationContextTransitionStyle = true
        UIViewController.loading.definesPresentationContext = true
        UIViewController.loading.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        UIViewController.loading.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(UIViewController.loading, animated: true, completion: nil)
    }
    
    func hideLoading(completion: (() -> Void)? = nil){
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(UIViewController.loadingAnimationDuration)) {
            UIViewController.loading.dismiss(animated: true, completion: completion)
        }
    }
}
