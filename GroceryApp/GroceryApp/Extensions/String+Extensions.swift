//
//  String+Extensions.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import Foundation
import UIKit

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "\(self)_comment")
    }
    
}
