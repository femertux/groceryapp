//
//  UITextField+Extensions.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import Foundation

import Foundation
import UIKit

extension UITextField {
    func setLeftImage(imageName:String) {
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: 48, height: 16))
        let imageView = UIImageView(frame: CGRect(x: 20, y: 0, width: 16, height: 16))
        imageView.image = UIImage(named: imageName)
        imageView.tintColor = .primaryColor
        outerView.addSubview(imageView)
        self.leftView = outerView
        self.leftViewMode = .always
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
