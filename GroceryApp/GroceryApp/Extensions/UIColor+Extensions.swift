//
//  UIColor+Extensions.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import Foundation
import UIKit

extension UIColor {
    static let backgroundColor = UIColor(named: "Background") ?? .black
    static let primaryColor = UIColor(named: "PrimaryColor") ?? .black
    static let primaryTextColor = UIColor(named: "PrimaryText") ?? .black
    static let secondaryTextColor = UIColor(named: "SecondaryText") ?? .black
    static let errorColor = UIColor(named: "RedColor") ?? .black
    static let borderColor = UIColor(named: "BorderColor") ?? .black
}
