//
//  UserDefaults+Extensions.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

import Foundation

extension UserDefaults {
    private enum Keys {
        static let users = "users"
        static let user = "user"
        static let cart = "cart"
    }
    
    class var users: Data? {
        get {
            return UserDefaults.standard.object(forKey: Keys.users) as? Data
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.users)
        }
    }
    
    class var user: Data? {
        get {
            return UserDefaults.standard.object(forKey: Keys.user) as? Data
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.user)
        }
    }
    
    class var cart: Data? {
        get {
            return UserDefaults.standard.object(forKey: Keys.cart) as? Data
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.cart)
        }
    }
}
