//
//  RequestError.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

enum RequestError: Error {
    case decode
    case invalidURL
    case noResponse
    case unauthorized
    case unexpectedStatusCode
    case notConnected
    case unknown
    
    var customMessage: String {
        switch self {
        case .decode:
            return "Decode error"
        case .unauthorized:
            return "Session expired"
        case .notConnected:
            return "Connection timeout"
        default:
            return "Unknown error"
        }
    }
}
