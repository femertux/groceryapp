//
//  HTTPClient.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

protocol HTTPClient {
    func sendRequest<T: Decodable>(endpoint: Endpoint, responseModel: T.Type, allowRetry: Bool) async -> Result<T, RequestError>
}

extension HTTPClient {
    func sendRequest<T: Decodable>(
        endpoint: Endpoint,
        responseModel: T.Type,
        allowRetry: Bool = true
    ) async -> Result<T, RequestError> {
        
        var urlComponents = URLComponents(string: endpoint.host)
        urlComponents?.scheme = endpoint.scheme
        urlComponents?.host = endpoint.host
        urlComponents?.path = endpoint.path
        
        guard let url = urlComponents?.url?.absoluteString.removingPercentEncoding else {
            return .failure(.invalidURL)
        }
        
        guard let requestUrl = URL(string: url) else {
            return .failure(.invalidURL)
        }
        
        var request = URLRequest(url: requestUrl)
        request.httpMethod = endpoint.method.rawValue
        request.allHTTPHeaderFields = endpoint.header
        
        if let body = endpoint.body {
            request.httpBody = try? JSONSerialization.data(withJSONObject: body, options: [])
        }
        
        do {
            let (data, response) = try await URLSession.shared.data(for: request, delegate: nil)
            guard let response = response as? HTTPURLResponse else {
                return .failure(.noResponse)
            }
            
            switch response.statusCode {
            case 200...299:
                do {
                    let decodedResponse = try JSONDecoder().decode(responseModel, from: data)
                    return .success(decodedResponse)
                } catch let error as NSError {
                    debugPrint(error)
                    return .failure(.decode)
                }
            case 401:
                return .failure(.unauthorized)
            default:
#if DEBUG
                print(String(data: data, encoding: .utf8)!)
                print("")
#endif
                return .failure(.unexpectedStatusCode)
            }
        } catch {
            if (error as! URLError).code == URLError.notConnectedToInternet {
                return .failure(.notConnected)
            } else {
                return .failure(.unknown)
            }
        }
    }
}
