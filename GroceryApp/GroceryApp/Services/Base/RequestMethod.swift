//
//  RequestMethod.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

enum RequestMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
