//
//  GroceryService.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

protocol GroceryServiceable {
    func getProducts() async -> Result<ProductsResponse, RequestError>
    func getProduct(id: String) async -> Result<ProductResponse, RequestError>
}

struct GroceryService: HTTPClient, GroceryServiceable {
    func getProducts() async -> Result<ProductsResponse, RequestError> {
        return await sendRequest(endpoint: GroceryEndpoints.products, responseModel: ProductsResponse.self)
    }
    
    func getProduct(id: String) async -> Result<ProductResponse, RequestError> {
        return await sendRequest(endpoint: GroceryEndpoints.product(id: id), responseModel: ProductResponse.self)
    }
}
