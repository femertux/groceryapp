//
//  GroceryEndpoints.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

enum GroceryEndpoints {
    case products
    case product(id: String)
}

extension GroceryEndpoints: Endpoint {
    
    var host: String {
        return "api.mocki.io"
    }
    
    var path: String {
        switch self {
        case .products:
            return "/v2/12b09ef4/obtenerProductos"
        case .product(let id):
            return "/v2/12b09ef4/product/" + id
        }
    }
    
    var method: RequestMethod {
        switch self {
        case .products, .product:
            return .get
        }
    }
    
    var header: [String: String]? {
        var data: [String:String] = [:]
        
        switch self {
        case .products, .product:
            data["Content-Type"] = "application/json"
            
        }
        return data
    }
    
    var body: [String: String]? {
        switch self {
        case .products, .product:
            return nil
        }
    }
}
