//
//  HomeController.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import UIKit

public protocol HomeDelegate {
    func loading()
    func showProducts()
    func showEmpty(text: String)
    func getShoppingCart(count: Int)
    func showError(message: String)
}


class HomeController: UIViewController {
    
    var router:Router!
    var viewModel: HomeViewModel?
    
    @IBOutlet weak var searchText: UITextField!
    @IBOutlet weak var productsTable: UITableView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        viewModel = HomeViewModel(
            groceryService: GroceryService(),groceryDefaults: GroceryUserDefaults(), delegate: self)
        
    }
    
    private func loadProduct(id: String){
        let parameters = ["id": id] as [String : String]
        self.router.show(view: .detail, sender: self,parameters: parameters)
    }
    
    @objc func showCart(_ sender: UITapGestureRecognizer) {
        self.router.show(view: .cart, sender: self)
    }
    
    @objc func logOut(_ sender: UITapGestureRecognizer) {
        let dialogMessage = UIAlertController(title: "grocery_market".localized, message: "Log out from Gocery Market?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Log out", style: .default, handler: { (action) -> Void in
            self.resetDefaults()
            self.navigationController?.popToRootViewController(animated: true)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
        }
        
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    private func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationItem.setHidesBackButton(true, animated: false)
        viewModel?.getProducts()
    }
}

extension HomeController {
    func setUpUI(){
        self.title = "grocery_market".localized
        
        let cart = UIBarButtonItem(
            image: UIImage(named: "cart"),
            style: .plain,
            target: self,
            action: #selector(self.showCart(_:))
        )
        
        let logout = UIBarButtonItem(
            image: UIImage(named: "figure.run.circle"),
            style: .plain,
            target: self,
            action: #selector(self.logOut(_:))
        )
        
        navigationItem.rightBarButtonItem = cart
        navigationItem.leftBarButtonItem = logout
        
        setUpLabel(emptyLabel,
                   text: "empty".localized,
                   textColor: .primaryTextColor,
                   textAlignment: .center,
                   font: emptyLabel.font,
                   numberOfLines: 2)
        
        let cellNib = UINib(nibName: "ProductCell", bundle: nil)
        productsTable.register(cellNib, forCellReuseIdentifier: "ProductCell")
        
        productsTable.dataSource = self
        productsTable.delegate = self
        
        searchText.delegate = self
        
        setUpTextField(searchText,
                       font: searchText.font!,
                       placeholder: "search".localized,
                       leftImage: "magnifyingglass",
                       keyboardType: .default,
                       returnKeyType: .search)
    }
}

extension HomeController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.getProductsCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 124
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductCell
        
        if let product = viewModel?.getProductByRow(row: indexPath.row){
            cell.setProduct(product: product)
        }
        
        cell.selectionStyle = .none
        cell.separatorInset = .zero
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let product = viewModel?.getProductByRow(row: indexPath.row){
            loadProduct(id: product.id ?? "")
        }
    }
}

extension HomeController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        guard let search = textField.text else { return true }
        emptyLabel.isHidden = true
        viewModel?.filterProducts(search: search)
        self.productsTable.reloadData()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        emptyLabel.isHidden = true
        viewModel?.filterProducts(search: "")
        self.productsTable.reloadData()
        return true
    }
}

extension HomeController: HomeDelegate {
    
    func loading() {
        showLoading()
    }
    
    func showProducts() {
        hideLoading(completion: {
            self.productsTable.reloadData()
        })
    }
    
    func getShoppingCart(count: Int) {
        DispatchQueue.main.async {
            if count > 0 {
                self.navigationItem.rightBarButtonItem?.image = UIImage(named: "cart.fill")
            } else {
                self.navigationItem.rightBarButtonItem?.image = UIImage(named: "cart")
            }
        }
    }
    
    func showError(message: String) {
        hideLoading(completion: {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(800)) {
                self.showAlertDialog(title: "grocery_market".localized, message: message)
            }
        })
    }
    
    func showEmpty(text: String) {
        emptyLabel.isHidden = false
        emptyLabel.text = "empty".localized + " \(text)"
    }
    
}
