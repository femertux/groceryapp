//
//  HomeViewModel.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

class HomeViewModel{
    
    private let groceryService: GroceryServiceable
    private var groceryDefaults: GroceryUserDefaultsProtocol
    private let delegate: HomeDelegate
    
    private var products : [Product] = []
    private var filteredProducts : [Product] = []
    private var isFiltering = false
    
    init(groceryService: GroceryServiceable,
         groceryDefaults: GroceryUserDefaultsProtocol,
         delegate: HomeDelegate) {
        self.groceryService = groceryService
        self.groceryDefaults = groceryDefaults
        self.delegate = delegate
    }
    
    func getProducts(){
        delegate.loading()
        Task {
            let result = await groceryService.getProducts()
            switch result {
            case .success(let response):
                if let products = response.products {
                    self.products = products
                    delegate.showProducts()
                }
            case .failure(_):
                delegate.showError(message: "A problem has occurred.\nTry again later")
            }
            
            getShoppingCart()
        }
    }
    
    func getProductsCount() -> Int {
        isFiltering ? filteredProducts.count : products.count
    }
    
    func getProductByRow(row: Int) -> Product {
        isFiltering ? filteredProducts[row] : products[row]
    }
    
    func isSearch() -> Bool {
        isFiltering
    }
    
    func filterProducts(search: String){
        if search.isEmpty {
            isFiltering = false
            filteredProducts = products
        } else {
            isFiltering = true
            filteredProducts = products.filter {
                $0.title?.range(of: search, options: .caseInsensitive, range: nil, locale: nil) != nil
            }
            
            if filteredProducts.isEmpty {
                delegate.showEmpty(text: search)
            }
        }
    }
    
    private func getShoppingCart(){
        let products = getLocalProducts()
        delegate.getShoppingCart(count: products.count)
    }
    
    private func getLocalProducts() -> [Product]{
        do {
            let products = try JSONDecoder().decode([Product].self, from: groceryDefaults.cart)
            return products
        } catch {
            debugPrint(error)
            return []
        }
    }
}
