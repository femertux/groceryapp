//
//  SplashController.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import UIKit

class SplashController: UIViewController {
    
    var router:Router!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func loadUser() -> UserBd? {
        let groceryDefaults = GroceryUserDefaults()
        do{
            return try JSONDecoder().decode(UserBd.self, from: groceryDefaults.user)
        } catch {
            debugPrint(error)
            return nil
        }
    }
    
    private func validateUser(){
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1800)) {
            if self.loadUser() != nil {
                self.router.show(view: .home, sender: self)
            } else {
                self.router.show(view: .login, sender: self)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        validateUser()
    }
}
