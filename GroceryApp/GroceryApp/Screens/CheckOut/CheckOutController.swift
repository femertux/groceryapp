//
//  CheckOutController.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import UIKit
import MapKit

class CheckOutController: UIViewController {
    
    var router:Router!
    
    @IBOutlet weak var deliveryTitleLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var checkOutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    @IBAction func checkOut(_ sender: Any) {
        showLoading()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(4000)) {
            self.hideLoading(completion: {
                let defaults = UserDefaults.standard
                defaults.removeObject(forKey: GroceryUserDefaults.keys.cart)
                let dialogMessage = UIAlertController(title: "grocery_market".localized, message: "Order Received\nYour payment has been sucessful", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                    
                    self.navigationController?.popToRootViewController(animated: true)
                })
                dialogMessage.addAction(ok)
                self.present(dialogMessage, animated: true, completion: nil)
            })
        }
    }
}

extension CheckOutController {
    func setUpUI(){
        self.title = "my_cart".localized
        setUpLabel(deliveryTitleLabel,
                   text: "delivery_address".localized,
                   textColor: .primaryTextColor,
                   font: deliveryTitleLabel.font)
        
        setUpLabel(paymentLabel,
                   text: "payment_method".localized,
                   textColor: .primaryTextColor,
                   font: paymentLabel.font)
        
        setUpLabel(cardLabel,
                   text: "Credit Card",
                   textColor: .primaryTextColor,
                   font: cardLabel.font)
        
        setUpLabel(numberLabel,
                   text: "1234 **** **** 6789",
                   textColor: .secondaryTextColor,
                   font: numberLabel.font)
        
        setUpButton(checkOutButton,
                    text: "pay".localized,
                    textColor: .white,
                    buttonColor: .primaryColor,
                    buttonBorderColor: .primaryColor,
                    font: (checkOutButton.titleLabel?.font)!)
        
        cardView.layer.cornerRadius = 8
        cardView.layer.shadowColor = UIColor.gray.cgColor
        cardView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cardView.layer.shadowRadius = 5.0
        cardView.layer.shadowOpacity = 0.4
        
        let annotations = MKPointAnnotation()
        annotations.title = "Destination"
        annotations.coordinate = CLLocationCoordinate2D(latitude: 12.140440, longitude: -86.213826)
        mapView.addAnnotation(annotations)
        
        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: annotations.coordinate.latitude, longitude: annotations.coordinate.longitude), span: span)
        mapView.setRegion(region, animated: true)
        
        mapView.isZoomEnabled = false
        mapView.isScrollEnabled = false
        mapView.isUserInteractionEnabled = false
        mapView.layer.cornerRadius = 8
    }
}
