//
//  DetailController.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import UIKit

public protocol DetailDelegate {
    func loading()
    func showProduct()
    func getShoppingCart(count: Int)
    func showError(message: String)
}

class DetailController: UIViewController {
    
    var router:Router!
    var viewModel: DetailViewModel?
    var parameters:[String:Any] = [:]
    private var product: Product!
    
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var unitMeasureLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var addCartButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var quantityStack: UIStackView!
    
    private var quantity = 0
    private var price = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        viewModel = DetailViewModel(
            groceryService: GroceryService(), groceryDefaults: GroceryUserDefaults(), delegate: self)
        viewModel?.getProduct(id: parameters["id"] as? String ?? "")
    }
    
    private func fillData(product: Product){
        if let title = product.title,
           let unitMeasure = product.unitMeasure,
           let detail = product.detail,
           let price = product.price,
           let image = product.image{
            
            titleLabel.text = title
            unitMeasureLabel.text = unitMeasure
            descriptionLabel.text = detail
            self.price = price
            updatePrice(price: price)
            updateQuantity()
            
            bannerImage.loadFrom(imageUrl: image)
            quantityStack.isHidden = false
            addCartButton.isHidden = false
        }
    }
    
    @IBAction func addToCart(_ sender: Any) {
        product.quantity = quantity
        viewModel?.addToCart(product: product)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func add(_ sender: Any) {
        quantity += 1
        updateQuantity()
        updatePrice(price: price)
    }
    
    @IBAction func remove(_ sender: Any) {
        guard quantity > 0 else { return }
        quantity -= 1
        updateQuantity()
        updatePrice(price: price)
    }
    
    private func updateQuantity(){
        quantityLabel.text = String(format: "%02d", quantity)
    }
    
    private func updatePrice(price: Double){
        priceLabel.text = String(format: "%.2f", price * Double((quantity == 0 ? 1 : quantity)))
    }
    
    @objc func showCart(_ sender: UITapGestureRecognizer) {
        self.router.show(view: .cart, sender: self)
    }
    
    @objc private func popToPrevious() {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationItem.setHidesBackButton(false, animated: false)
    }
}

extension DetailController {
    func setUpUI(){
        
        navigationItem.backBarButtonItem = UIBarButtonItem(
            title: "",
            style: .plain,
            target: self,
            action: #selector(popToPrevious)
        )
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "cart"),
            style: .plain,
            target: self,
            action: #selector(self.showCart(_:))
        )
        
        bannerImage.contentMode = .scaleAspectFit
        
        setUpLabel(titleLabel,
                   text: "",
                   textColor: .primaryTextColor,
                   font: titleLabel.font,
                   numberOfLines: 2)
        
        setUpLabel(unitMeasureLabel,
                   text: "",
                   textColor: .secondaryTextColor,
                   font: unitMeasureLabel.font)
        
        setUpLabel(descriptionLabel,
                   text: "",
                   textColor: .secondaryTextColor,
                   textAlignment: .justified,
                   font: descriptionLabel.font,
                   numberOfLines: 0)
        
        setUpLabel(priceLabel,
                   text: "",
                   textColor: .primaryTextColor,
                   font: priceLabel.font)
        
        setUpLabel(quantityLabel,
                   text: "",
                   textColor: .primaryTextColor,
                   textAlignment: .center,
                   font: quantityLabel.font)
        
        setUpButton(addCartButton,
                    text: "add_to_cart".localized,
                    textColor: .white,
                    buttonColor: .primaryColor,
                    buttonBorderColor: .primaryColor,
                    font: (addCartButton.titleLabel?.font)!)
        
        setUpButton(addButton,
                    text: "+",
                    textColor: .white,
                    buttonColor: .primaryColor,
                    buttonBorderColor: .primaryColor,
                    font: (addButton.titleLabel?.font)!)
        
        setUpButton(removeButton,
                    text: "-",
                    textColor: .white,
                    buttonColor: .primaryColor,
                    buttonBorderColor: .primaryColor,
                    font: (removeButton.titleLabel?.font)!)
        
    }
}

extension DetailController: DetailDelegate {
    func loading() {
        showLoading()
    }
    
    func showProduct() {
        hideLoading(completion: {
            guard let product = self.viewModel?.getProductDetail() else { return }
            self.product = product
            self.quantity = product.quantity ?? 0
            self.price = product.price ?? 0.0
            self.fillData(product: product)
            
            if self.quantity > 0 {
                self.addCartButton.setTitle("update_cart".localized, for: .normal)
            }
        })
    }
    
    func getShoppingCart(count: Int) {
        DispatchQueue.main.async {
            if count > 0 {
                self.navigationItem.rightBarButtonItem?.image = UIImage(named: "cart.fill")
            } else {
                self.navigationItem.rightBarButtonItem?.image = UIImage(named: "cart")
            }
        }
    }
    
    func showError(message: String) {
        hideLoading(completion: {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(800)) {
                self.showAlertDialog(title: "grocery_market".localized, message: message)
            }
        })
    }
}
