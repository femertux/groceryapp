//
//  DetailViewModel.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

class DetailViewModel{
    
    private let groceryService: GroceryServiceable
    private var groceryDefaults: GroceryUserDefaultsProtocol
    private let delegate: DetailDelegate
    
    private var product: Product!
    private var products: [Product] = []
    
    init(groceryService: GroceryServiceable,
         groceryDefaults: GroceryUserDefaultsProtocol,
         delegate: DetailDelegate) {
        self.groceryService = groceryService
        self.groceryDefaults = groceryDefaults
        self.delegate = delegate
    }
    
    func getProduct(id: String){
        delegate.loading()
        Task {
            let result = await groceryService.getProduct(id: id)
            switch result {
            case .success(let response):
                if let product = response.product {
                    self.product = product
                    
                    if let local = getLocalProduct(id: id) {
                        self.product.quantity = local.quantity
                    }
                    
                    getShoppingCart()
                    delegate.showProduct()
                }
            case .failure(_):
                delegate.showError(message: "A problem has occurred.\nTry again later")
            }
        }
    }
    
    func addToCart(product: Product){
        saveProduct(product: product)
    }
    
    func getProductDetail() -> Product {
        return product
    }
    
    private func saveProduct(product: Product){
        do {
            var products = getLocalProducts()
            if let row = products.firstIndex(where: {$0.id == product.id}) {
                if product.quantity == 0 {
                    products.remove(at: row)
                } else {
                    products[row] = product
                }
            } else {
                products.append(product)
            }
            
            let encodedData = try JSONEncoder().encode(products)
            groceryDefaults.cart = encodedData
        } catch {
            debugPrint(error)
        }
    }
    
    private func getShoppingCart(){
        let products = getLocalProducts()
        delegate.getShoppingCart(count: products.count)
    }
    
    private func getLocalProduct(id: String) -> Product? {
        let products = getLocalProducts()
        return products.first(where: { $0.id == id })
    }
    
    private func getLocalProducts() -> [Product]{
        do {
            let products = try JSONDecoder().decode([Product].self, from: groceryDefaults.cart)
            return products
        } catch {
            debugPrint(error)
            return []
        }
    }
}
