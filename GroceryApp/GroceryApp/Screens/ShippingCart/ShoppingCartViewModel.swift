//
//  ShoppingCartViewModel.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

class ShoppingCartViewModel{
    
    private var groceryDefaults: GroceryUserDefaultsProtocol
    private let delegate: ShoppingCartDelegate
    private var products: [Product] = []
    
    init(groceryDefaults: GroceryUserDefaultsProtocol,
         delegate: ShoppingCartDelegate) {
        self.groceryDefaults = groceryDefaults
        self.delegate = delegate
    }
    
    func getShoppingCart() {
        products = getLocalProducts()
        delegate.showProducts()
    }
    
    func getProductsCount() -> Int {
        products.count
    }
    
    func getProductByRow(row: Int) -> Product {
        products[row]
    }
    
    func getProducts() -> [Product] {
        products
    }
    
    func getProductById(id: String) -> Product? {
        products.first(where: {$0.id == id})
    }
    
    func updateProduct(product: Product){
        saveProduct(product: product)
    }
    
    private func saveProduct(product: Product){
        do {
            var products = getLocalProducts()
            if let row = products.firstIndex(where: {$0.id == product.id}) {
                if product.quantity == 0 {
                    products.remove(at: row)
                } else {
                    products[row] = product
                }
            } else {
                products.append(product)
            }
            
            let encodedData = try JSONEncoder().encode(products)
            groceryDefaults.cart = encodedData
        } catch {
            debugPrint(error)
        }
        
        products = getLocalProducts()
    }
    
    private func getLocalProducts() -> [Product]{
        do {
            let products = try JSONDecoder().decode([Product].self, from: groceryDefaults.cart)
            return products
        } catch {
            debugPrint(error)
            return []
        }
    }
}
    
