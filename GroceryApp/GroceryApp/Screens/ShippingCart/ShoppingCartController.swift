//
//  ShoppingCartController.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import UIKit

public protocol ShoppingCartDelegate {
    func loading()
    func showProducts()
    func showError(message: String)
}

class ShoppingCartController: UIViewController {
    
    var router:Router!
    var viewModel: ShoppingCartViewModel?

    @IBOutlet weak var productsTable: UITableView!
    @IBOutlet weak var billTitleLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalValueLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var feeValueLabel: UILabel!
    @IBOutlet weak var totalTaxLabel: UILabel!
    @IBOutlet weak var totalTaxValueLabel: UILabel!
    @IBOutlet weak var checkOutButton: UIButton!
    @IBOutlet weak var productsConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        viewModel = ShoppingCartViewModel(
            groceryDefaults: GroceryUserDefaults(), delegate: self)
        viewModel?.getShoppingCart()
    }

    @IBAction func checkOut(_ sender: Any) {
        self.router.show(view: .checkout, sender: self)
    }
}

extension ShoppingCartController {
    func setUpUI(){
        self.title = "my_cart".localized
        
        let cellNib = UINib(nibName: "ProductCell", bundle: nil)
        productsTable.register(cellNib, forCellReuseIdentifier: "ProductCell")
        
        productsTable.dataSource = self
        productsTable.delegate = self
        
        setUpLabel(billTitleLabel,
                   text: "bill_details".localized,
                   textColor: .primaryTextColor,
                   font: billTitleLabel.font)
        
        setUpLabel(totalLabel,
                   text: "item_total".localized,
                   textColor: .secondaryTextColor,
                   font: totalLabel.font)
        
        setUpLabel(feeLabel,
                   text: "taxes".localized,
                   textColor: .secondaryTextColor,
                   font: feeLabel.font)
        
        setUpLabel(totalTaxLabel,
                   text: "order_total".localized,
                   textColor: .secondaryTextColor,
                   font: totalTaxLabel.font)
        
        setUpLabel(totalValueLabel,
                   text: "",
                   textColor: .primaryTextColor,
                   font: totalValueLabel.font)
        
        setUpLabel(feeValueLabel,
                   text: "",
                   textColor: .primaryTextColor,
                   font: feeValueLabel.font)
        
        setUpLabel(totalTaxValueLabel,
                   text: "",
                   textColor: .primaryTextColor,
                   font: totalTaxValueLabel.font)
        
        setUpButton(checkOutButton,
                    text: "checkout".localized,
                    textColor: .white,
                    buttonColor: .primaryColor,
                    buttonBorderColor: .primaryColor,
                    font: (checkOutButton.titleLabel?.font)!)
    }
    
    func calculateBill(products: [Product]){
        var total = 0.0
        var tax = 0.0
        products.forEach{
            total += ($0.price ?? 0.0) * Double($0.quantity ?? 1)
            tax += ($0.tax ?? 0.0) * Double($0.quantity ?? 1)
        }
        
        let totalBill = total + tax
        
        totalValueLabel.text = String(format: "$%.2f", total)
        feeValueLabel.text = String(format: "+ $%.2f", tax)
        totalTaxValueLabel.text = String(format: "+ $%.2f", totalBill)
    }
}

extension ShoppingCartController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = viewModel?.getProductsCount() ?? 0
        productsConstraint.constant = CGFloat(count * 130)
        return count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 124
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductCell
        
        if let product = viewModel?.getProductByRow(row: indexPath.row){
            cell.setProduct(product: product, edit: true)
            cell.delegate = self
        }
        
        cell.selectionStyle = .none
        cell.separatorInset = .zero
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension ShoppingCartController: ShoppingCartDelegate {
    func loading() {
        showLoading()
    }
    
    func showProducts() {
        hideLoading(completion: {
            self.productsTable.reloadData()
            self.calculateBill(products: self.viewModel?.getProducts() ?? [])
        })
    }
    
    func showError(message: String) {
        hideLoading(completion: {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(800)) {
                self.showAlertDialog(title: "grocery_market".localized, message: message)
            }
        })
    }
}


extension ShoppingCartController: ProductCellDelegate {
    func add(id: String) {
        if var product = viewModel?.getProductById(id: id) {
            product.quantity! += 1
            viewModel?.updateProduct(product: product)
            calculateBill(products: self.viewModel?.getProducts() ?? [])
        }
    }
    
    func remove(id: String) {
        if var product = viewModel?.getProductById(id: id) {
            product.quantity! -= 1
            viewModel?.updateProduct(product: product)
            calculateBill(products: self.viewModel?.getProducts() ?? [])
            
            if product.quantity == 0 {
                viewModel?.getShoppingCart()
            }
        }
    }
    
}
