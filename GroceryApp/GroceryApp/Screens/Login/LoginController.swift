//
//  LoginController.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import UIKit
import LocalAuthentication

public protocol LoginDelegate {
    func loading()
    func goToHome()
    func showError(message: String)
}

class LoginController: UIViewController {
    
    var router:Router!
    var viewModel: LoginViewModel?
    
    @IBOutlet weak var userText: UITextField!
    @IBOutlet weak var userErrorLabel: UILabel!
    @IBOutlet weak var passwordText: PasswordTextField!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var stackTouch: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var touchLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var signInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        
        viewModel = LoginViewModel(
            groceryDefaults: GroceryUserDefaults(),
            delegate: self)
    }
    
    @IBAction func signIn(_ sender: Any) {
        self.view.endEditing(true)
        userErrorLabel.isHidden = true
        passwordErrorLabel.isHidden = true
        
        guard let userName = userText.text else { return }
        
        guard let password = passwordText.text else { return }
        
        guard userName.isEmpty == false else {
            userErrorLabel.text = "username_error".localized
            userErrorLabel.isHidden = false
            return
        }
        
        guard password.isEmpty == false else {
            passwordErrorLabel.text = "password_error".localized
            passwordErrorLabel.isHidden = false
            return
        }
        
        viewModel?.loginUser(username: userName, password: password)
    }
    
    @objc func signInBiometric() {
        let context = LAContext()
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error){
            
            let reason = "Touch ID for appName"
        
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) {
                [weak self] success, authenticationError in
                DispatchQueue.main.async {
                    if success {
                        self?.viewModel?.loginBiometric()
                    } else {
                        self?.showError(message: "You could not be verified; please try again.")
                    }
                }
            }
        } else {
            stackTouch.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        navigationItem.setHidesBackButton(true, animated: false)
    }
}

extension LoginController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == userText {
            passwordText.becomeFirstResponder()
        } else {
            signInButton.sendActions(for: .touchUpInside)
        }
        return true
    }
}

extension LoginController {
    func setUpUI(){
        setUpLabel(titleLabel,
                   text: "grocery_market".localized,
                   textColor: .primaryTextColor,
                   font: titleLabel.font)
        
        setUpLabel(messageLabel,
                   text: "sign_in_to_continue".localized,
                   textColor: .primaryTextColor,
                   font: messageLabel.font)
        
        setUpLabel(touchLabel,
                   text: "use_touch_id".localized,
                   textColor: .primaryTextColor,
                   font: touchLabel.font)
        
        setUpLabel(userErrorLabel,
                   text: "username_error".localized,
                   textColor: .errorColor,
                   font: userErrorLabel.font)
        
        setUpLabel(passwordErrorLabel,
                   text: "password_error".localized,
                   textColor: .errorColor,
                   font: passwordErrorLabel.font)
        
        setUpButton(signInButton,
                    text: "sign_in".localized,
                    textColor: .white,
                    buttonColor: .primaryColor,
                    buttonBorderColor: .primaryColor,
                    font: (signInButton.titleLabel?.font)!)
        
        setUpTextField(userText,
                       font: userText.font!,
                       placeholder: "username".localized,
                       keyboardType: .emailAddress,
                       returnKeyType: .next)
        
        setUpTextField(passwordText,
                       font: passwordText.font!,
                       placeholder: "password".localized,
                       returnKeyType: .done,
                       isSecureTextEntry: true)
        
        stackTouch.isUserInteractionEnabled = true
        stackTouch.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(signInBiometric)))
    }
}

extension LoginController: LoginDelegate {
    func loading() {
        showLoading()
    }
    
    func goToHome() {
        hideLoading(completion: {
            self.router.show(view: .home, sender: self)
        })
    }
    
    func showError(message: String) {
        hideLoading(completion: {
            self.showAlertDialog(title: "grocery_market".localized, message: message)
        })
    }
    
}
