//
//  LoginViewModel.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

class LoginViewModel{
    
    private var groceryDefaults: GroceryUserDefaultsProtocol
    private let delegate: LoginDelegate
    
    init(groceryDefaults: GroceryUserDefaultsProtocol,
         delegate: LoginDelegate) {
        self.groceryDefaults = groceryDefaults
        self.delegate = delegate
        generateUsers()
    }
    
    func loginUser(username:String,password:String){
        delegate.loading()
        let usersData = groceryDefaults.users
        do{
            let users = try JSONDecoder().decode([UserBd].self, from: usersData)
            if let user = validateUser(username: username, password: password, users: users) {
                saveUser(user: user)
                delegate.goToHome()
            }else {
                delegate.showError(message: "username_error".localized)
            }
        } catch {
            debugPrint(error)
        }
    }
    
    func loginBiometric(){
        delegate.loading()
        saveUser(user: UserBd(username: "", password: "", biometric: true))
        delegate.goToHome()
    }
    
    private func validateUser(username:String,password:String, users: [UserBd]) -> UserBd? {
        return users.first { $0.username == username && $0.password == password }
    }
    
    private func saveUser(user: UserBd){
        do {
            let encodedData = try JSONEncoder().encode(user)
            groceryDefaults.user = encodedData
        } catch {
            debugPrint(error)
        }
    }
    
    private func generateUsers(){
        let users: [UserBd] = [
            UserBd(username: "UserTest", password: "abc12345")
        ]
        
        do {
            let encodedData = try JSONEncoder().encode(users)
            groceryDefaults.users = encodedData
        } catch {
            debugPrint(error)
        }
    }
}
