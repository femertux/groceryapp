//
//  Router.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import Foundation
import UIKit

class Router {
    enum Scene {
        case splash
        case login
        case home
        case detail
        case cart
        case checkout
    }
    
    func show(view: Scene, sender: UIViewController, parameters:[String:Any] = [:]) {
        switch view {
        case .splash:
            splash(sender: sender, parameters: parameters)
            break
        case .login:
            login(sender: sender, parameters: parameters)
            break
        case .home:
            home(sender: sender, parameters: parameters)
            break
        case .detail:
            detail(sender: sender, parameters: parameters)
            break
        case .cart:
            shoppingCart(sender: sender, parameters: parameters)
        case .checkout:
            checkOut(sender: sender, parameters: parameters)
            break
        }
    }
    
    private func show(target: UIViewController, sender: UIViewController) {
        
        if let nav = sender as? UINavigationController {
            //push root controller on navigation stack
            nav.pushViewController(target, animated: true)
            nav.setNavigationBarHidden(true, animated: true)
            return
        }
        
        if let nav = sender.navigationController {
            //add controller to navigation stack
            nav.pushViewController(target, animated: true)
            
            
        } else {
            //present modally
            sender.present(target, animated: true, completion: nil)
        }
    }
    
    func pop(sender: UIViewController){
        if let nav = sender.navigationController {
            //add controller to navigation stack
            nav.popViewController(animated: true)
        } else {
            //present modally
            sender.dismiss(animated: true, completion: nil)
        }
    }
    
    func splash(sender: UIViewController,parameters:[String:Any]){
        let splashController = SplashController(nibName: "SplashController", bundle: nil)
        splashController.router = self
        self.show(target: splashController, sender: sender)
    }
    
    func login(sender: UIViewController,parameters:[String:Any]){
        let loginController = LoginController(nibName: "LoginController", bundle: nil)
        loginController.router = self
        self.show(target: loginController, sender: sender)
    }
    
    func home(sender: UIViewController,parameters:[String:Any]){
        let homeController = HomeController(nibName: "HomeController", bundle: nil)
        homeController.router = self
        self.show(target: homeController, sender: sender)
    }
    
    func detail(sender: UIViewController,parameters:[String:Any]){
        let detailController = DetailController(nibName: "DetailController", bundle: nil)
        detailController.router = self
        detailController.parameters = parameters
        self.show(target: detailController, sender: sender)
    }
    
    func shoppingCart(sender: UIViewController,parameters:[String:Any]){
        let shoppingCartController = ShoppingCartController(nibName: "ShoppingCartController", bundle: nil)
        shoppingCartController.router = self
        //videoDetailController.parameters = parameters
        self.show(target: shoppingCartController, sender: sender)
    }
    
    func checkOut(sender: UIViewController,parameters:[String:Any]){
        let checkOutController = CheckOutController(nibName: "CheckOutController", bundle: nil)
        checkOutController.router = self
        //videoDetailController.parameters = parameters
        self.show(target: checkOutController, sender: sender)
    }
}
