//
//  PasswordTextField.swift
//  GroceryApp
//
//  Created by Femer Garcia on 8/8/23.
//

import Foundation
import UIKit

class PasswordTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.isSecureTextEntry = true
        
        //show/hide button
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: 42, height: 26))
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 26, height: 26))
        button.setImage(UIImage(named: "eye.slash")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.setImage(UIImage(named: "eye")?.withRenderingMode(.alwaysTemplate), for: .selected)
        
        outerView.addSubview(button)
        rightView = outerView
        rightViewMode = .always
        button.addTarget(self, action: #selector(showHidePassword(_:)), for: .touchUpInside)
        button.tintColor = .lightGray
    }
    
    @objc private func showHidePassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.isSecureTextEntry = !sender.isSelected
        sender.tintColor = sender.isSelected ? .primaryColor : .lightGray
    }
    
}
