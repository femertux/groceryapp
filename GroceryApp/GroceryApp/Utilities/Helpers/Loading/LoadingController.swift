//
//  LoadingController.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import UIKit

class LoadingController: UIViewController {

    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        loadingView.layer.cornerRadius = 10
        loadingView.layer.shadowColor = UIColor.gray.cgColor
        loadingView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        loadingView.layer.shadowRadius = 5.0
        loadingView.layer.shadowOpacity = 0.4
    }


}
