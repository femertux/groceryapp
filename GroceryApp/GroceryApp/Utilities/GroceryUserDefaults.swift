//
//  GroceryUserDefaults.swift
//  GroceryApp
//
//  Created by Femer Garcia on 9/8/23.
//

import Foundation

protocol GroceryUserDefaultsProtocol {
    var users: Data { get set }
    var user: Data { get set }
    var cart: Data { get set }
}

struct GroceryUserDefaults: GroceryUserDefaultsProtocol {
    enum keys {
        static let users = "users"
        static let user = "user"
        static let cart = "cart"
    }
    
    var users: Data {
        get { UserDefaults.users ?? Data() }
        set { UserDefaults.users = newValue }
    }
    
    var user: Data {
        get { UserDefaults.user ?? Data() }
        set { UserDefaults.user = newValue }
    }
    
    
    var cart: Data {
        get { UserDefaults.cart ?? Data() }
        set { UserDefaults.cart = newValue }
    }
}
